﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarioManager : MonoBehaviour {
    private int currentLifeNumber = 3;
    private List<BoxCollider2D> currentlyCollidingWith = new List<BoxCollider2D>();

    private List<Sprite> marioSpriteStates;
    private int currentMarioSpriteState = 0;

    public float marioAnimationSwitchTime = 0.5f;
    private float currentMarioAnimationSwitchTime;

    private GameObject gameOver, gameWin;

    [SerializeField]
    private bool isJumping = false;

    public float movementSpeed = 0.05f;

    public AudioClip lostALife;
    public AudioClip downingInLava;
    public AudioClip gameWinner;

    public GameObject fireParticleForLava;
    public GameObject fireParticleForGhoul;

    private void Start() {
        currentMarioAnimationSwitchTime = marioAnimationSwitchTime;

        marioSpriteStates = TileManager.instance.marioSpriteStatesList;

        //Logic for setting up Lives Remaining UI.
        TileManager.instance.loadLevel.tmpLivesRemaining.text = "Lives Remaining: " + currentLifeNumber.ToString();

        gameOver = GameObject.FindGameObjectWithTag("GameOver");
        gameWin = GameObject.FindGameObjectWithTag("GameWin");

        gameOver.SetActive(false);
        gameWin.SetActive(false);

        Camera.main.GetComponent<CameraFollowPlayer>().mario = this.gameObject;

        for (int i = 0; i < FindObjectsOfType<EnemyFlying>().Length; ++i) {
            FindObjectsOfType<EnemyFlying>()[i].mario = this.gameObject;
        }
    }

    private void Update() {
        if (currentlyCollidingWith.Count > 0) {
            foreach (BoxCollider2D justCollidedWith in currentlyCollidingWith) {
                if (justCollidedWith != null) {
                    if (GetComponent<BoxCollider2D>().IsTouching(justCollidedWith)) {
                        justCollidedWith.GetComponent<Tile>().grndTimerCurrent -= Time.deltaTime;
                    }
                }
            }
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) {
            gameObject.transform.position += (Vector3.right * movementSpeed);
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) {
            gameObject.transform.position += (Vector3.left * movementSpeed);
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }

        currentMarioAnimationSwitchTime -= Time.deltaTime;

        if (!isJumping && !Input.anyKey) {
            currentMarioAnimationSwitchTime = marioAnimationSwitchTime;
            gameObject.GetComponent<SpriteRenderer>().sprite = marioSpriteStates[0];
        } else if (isJumping) {
            gameObject.GetComponent<SpriteRenderer>().sprite = marioSpriteStates[1];
        } else {
            if (currentMarioAnimationSwitchTime <= 0.0f) {
                currentMarioAnimationSwitchTime = marioAnimationSwitchTime;
                currentMarioSpriteState = (currentMarioSpriteState < marioSpriteStates.Count - 1) ? currentMarioSpriteState + 1 : 0;
                gameObject.GetComponent<SpriteRenderer>().sprite = marioSpriteStates[currentMarioSpriteState];
            }
        }

        if (!isJumping) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0.0f, 250.0f));
                isJumping = true;
            }
        }
    }

    private void ResetLevel() {
        currentlyCollidingWith.Clear();

        TileManager.instance.marioWasLastSeenHere = Vector3.zero;

        isJumping = false;

        TileManager.instance.canExecute = false;

        //Remove all tiles
        foreach (GameObject tile in TileManager.instance.tiles) {
            Destroy(tile.gameObject);
        }

        //Reset tiles list
        TileManager.instance.tiles.Clear();

        //Reset lavaTiles list
        TileManager.instance.lavaTiles.Clear();

        //Suicide enemies
        foreach(GameObject enemyToDelete in GameObject.FindGameObjectsWithTag("Enemy")) {
            Destroy(enemyToDelete.gameObject);
        }

        //Re-do tile placement
        TileManager.instance.loadLevel.PlaceTiles("Level_1");

        gameObject.transform.position = TileManager.instance.marioWasLastSeenHere;
        gameObject.GetComponent<SpriteRenderer>().flipX = false;
        TileManager.instance.marioWasLastSeenHere = Vector3.zero;

        for (int i = 0; i < FindObjectsOfType<EnemyFlying>().Length; ++i) {
            FindObjectsOfType<EnemyFlying>()[i].mario = this.gameObject;
        }

        currentLifeNumber--;

        //Logic for updating Lives Remaining UI.
        TileManager.instance.loadLevel.tmpLivesRemaining.text = "Lives Remaining: " + currentLifeNumber.ToString();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.name.Contains("Fireball")) {
            collision.GetComponent<Fireball>().SelfDestruct();
        }

        //Special case for if LAVA killed Mario.
        if (collision.name.Contains("LAVA")) {
            TileManager.instance.marioWasLastSeenHere = new Vector3(gameObject.transform.position.x, Mathf.Round(gameObject.transform.position.y), gameObject.transform.position.z);
            GameObject tempObj = Instantiate(fireParticleForLava, new Vector3(gameObject.transform.position.x, Mathf.Round(gameObject.transform.position.y), gameObject.transform.position.z), Quaternion.identity) as GameObject;
        }

        if (collision.name.Contains("END!")) {
            gameWin.SetActive(true);

            GetComponent<AudioSource>().PlayOneShot(gameWinner, 1.0f);

            //Remove all dangers from the level, the player may want to "Mine" the level for fun, in celebration of winning!
            //Remove LAVA tiles
            for (int i = TileManager.instance.lavaTiles.Count - 1; i > 0; --i) {
                Destroy(TileManager.instance.lavaTiles[i]);
            }

            //Clear the lavaTiles list.
            TileManager.instance.lavaTiles.Clear();

            //Suicide enemies
            foreach (GameObject enemyToDelete in GameObject.FindGameObjectsWithTag("Enemy")) {
                Destroy(enemyToDelete.gameObject);
            }

        } else {
            if (currentLifeNumber <= 1) {
                //Update Lives counter
                currentLifeNumber = 0;

                //Logic for updating Lives Remaining UI.
                TileManager.instance.loadLevel.tmpLivesRemaining.text = "Lives Remaining: " + currentLifeNumber.ToString();

                GameObject tempObj = Instantiate(TileManager.instance.EMPTTile, new Vector3(gameObject.transform.position.x, Mathf.Round(gameObject.transform.position.y), gameObject.transform.position.z), Quaternion.identity) as GameObject;
                TileManager.instance.tiles[TileManager.instance.marioPositionInList] = tempObj;

                Camera.main.GetComponent<CameraFollowPlayer>().playGameOverSoundEffect = true;

                gameOver.SetActive(true);

                //Ran out of lives, so stay dead.
                Destroy(this.gameObject);

            } else {
                if (collision.name.Contains("LAVA")) {
                    GetComponent<AudioSource>().PlayOneShot(downingInLava);
                } else {
                    GetComponent<AudioSource>().PlayOneShot(lostALife);
                }

                ResetLevel();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.name.Contains("XGND") || collision.collider.name.Contains("XFLY")) {
            if (currentLifeNumber <= 1) {
                //Update Lives counter to 0.
                currentLifeNumber = 0;

                //Logic for updating Lives Remaining UI.
                TileManager.instance.loadLevel.tmpLivesRemaining.text = "Lives Remaining: " + currentLifeNumber.ToString();

                GameObject tempObj = Instantiate(TileManager.instance.EMPTTile, new Vector3(gameObject.transform.position.x, Mathf.Round(gameObject.transform.position.y), gameObject.transform.position.z), Quaternion.identity) as GameObject;
                TileManager.instance.tiles[TileManager.instance.marioPositionInList] = tempObj;

                gameOver.SetActive(true);

                Camera.main.GetComponent<CameraFollowPlayer>().playGameOverSoundEffect = true;

                //Ran out of lives, so stay dead.
                Destroy(this.gameObject);

            } else {
                //if(collision.collider.name.Contains("LAVA")) {
                //    GetComponent<AudioSource>().PlayOneShot(downingInLava);
                //} else {
                //    GetComponent<AudioSource>().PlayOneShot(lostALife);
                //}

                GameObject tempObj = Instantiate(fireParticleForGhoul, new Vector3(gameObject.transform.position.x, Mathf.Round(gameObject.transform.position.y), gameObject.transform.position.z), Quaternion.identity) as GameObject;

                ResetLevel();
            }
        }


        if (collision.collider.GetComponent<Tile>() != null) {
            if (collision.collider.GetComponent<Tile>().isGRNDTile) {
                if (isJumping) {
                    collision.collider.GetComponent<Tile>().currentGRNDTileFrame += 1;
                    if (collision.collider.GetComponent<Tile>().currentGRNDTileFrame <= collision.collider.GetComponent<Tile>().grndTileFrames.Count) {
                        collision.collider.GetComponent<SpriteRenderer>().sprite = collision.collider.GetComponent<Tile>().grndTileFrames[collision.collider.GetComponent<Tile>().currentGRNDTileFrame - 1];
                    } else {
                        foreach (BoxCollider2D colliderToRemove in currentlyCollidingWith) {
                            if (collision.collider.GetComponent<BoxCollider2D>() == colliderToRemove) {
                                currentlyCollidingWith.Remove(colliderToRemove);
                            }
                        }

                        Destroy(collision.collider.gameObject);
                    }
                }

                if (collision.collider != null) {
                    currentlyCollidingWith.Add(collision.collider.GetComponent<BoxCollider2D>());
                }
            }

            if (collision.collider.GetComponent<Tile>().isGRNDTile || collision.collider.GetComponent<Tile>().isEDGETile) {
                if (isJumping) {
                    isJumping = false;
                }
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision) {
        if (collision.collider.GetComponent<Tile>() != null) {
            if (collision.collider.GetComponent<Tile>().isGRNDTile) {
                for (int i = 0; i < currentlyCollidingWith.Count; ++i) {
                    if (collision.collider.GetComponent<BoxCollider2D>() == currentlyCollidingWith[i]) {
                        currentlyCollidingWith.Remove(currentlyCollidingWith[i]);
                    }
                }
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision) {

    }
}