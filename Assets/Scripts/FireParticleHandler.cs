﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireParticleHandler : MonoBehaviour
{
    private float timeTillDeath = 2.0f;
    private bool doFire = true, nowWait = false;

    private void Awake() {
        GetComponent<ParticleSystem>().Play();
    }

    private void Update() {
        if(doFire) {
            if (timeTillDeath > 0.0f) {
                timeTillDeath -= Time.deltaTime;
            } else {
                GetComponent<ParticleSystem>().Stop();
                nowWait = true;
                doFire = false;
                timeTillDeath = 2.0f;
            }
        }

        if(nowWait) {
            if (timeTillDeath > 0.0f) {
                timeTillDeath -= Time.deltaTime;
            } else {
                Destroy(this.gameObject);
            }
        }
    }
}
