﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {
    //Singleton instancing
    public static TileManager instance {
        get; private set;
    }

    [HideInInspector]
    public LoadLevel loadLevel;

    [HideInInspector]
    public readonly int xTiles = 100, yTiles = 17;

    //These will be passed on to instantiated tiles that are type GRND.
    public List<Sprite> grndTileFrames = new List<Sprite>();

    //[HideInInspector]
    public List<GameObject> tiles = new List<GameObject>();

    [HideInInspector]
    public List<GameObject> lavaTiles = new List<GameObject>();

    public List<Sprite> marioSpriteStatesList = new List<Sprite>();

    public Transform allTileParent;

    public GameObject lavaTile;
    public GameObject EMPTTile;

    private float lavaTimerCurrent;
    public float lavaTimer = 0.5f;

    [HideInInspector]
    public int marioPositionInList;

    [HideInInspector]
    public Vector3 marioWasLastSeenHere = Vector3.zero;

    [HideInInspector] //Stops the Update() method from continuing when it is GameOver.
    public bool canExecute = true;


    //Checks for singleton validity
    void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Debug.Log("Instance of " + this.name + " already existed. New instance has been destroyed.");
            Destroy(this.gameObject);
        }
    }

    private void Start() {
        lavaTimerCurrent = lavaTimer;
        loadLevel = FindObjectOfType<LoadLevel>();
    }

    private void Update() {
        if (canExecute) {
            if (lavaTimerCurrent <= 0.0f) {
                //Temprarily duplicate lavaTiles list and use that duplicate locally.
                List<GameObject> tempLavaList = lavaTiles;

                //Go through all lava tiles and update them.
                for (int i = 0; i < tempLavaList.Count; ++i) {
                    //If there is LAVA below this current LAVA being checked, ignore/skip the check for this one.
                    if ((tempLavaList[i].GetComponent<Tile>().tileNumberInList) + xTiles <= xTiles * yTiles) {
                        if (tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) + xTiles].name.Contains("LAVA")) {
                            continue;
                        }
                    }

                    if ((tempLavaList[i].GetComponent<Tile>().tileNumberInList) + xTiles <= xTiles * yTiles) {
                        //If below this LAVA tile, there is a tile that is able to proagate LAVA, then check if we can propagate the LAVA.
                        if (tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) + xTiles].name.Contains("GRND") || tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) + xTiles].name.Contains("EDGE")) {

                            //lavaTiles.Add(tempObj) wants an assigned GameObject.
                            GameObject tempObj;

                            if (tempLavaList[i].GetComponent<Tile>().tileNumberInList > 0) {
                                //Check to the left of the tile that can propagate LAVA.
                                if (tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) - 1].name.Contains("EMPT")) {
                                    tempObj = Instantiate(lavaTile, tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) - 1].transform.position, Quaternion.identity) as GameObject;

                                    if (tempObj.transform.position == new Vector3(0.0f, 9.0f, 0.0f)) {
                                        tempObj.transform.position = marioWasLastSeenHere;
                                    }

                                    //Replace the EMPT tile in the tiles list with LAVA.
                                    tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) - 1] = tempObj;

                                    //Update the tileNumberInLavaList for the current added LAVA.
                                    tempLavaList[i].GetComponent<Tile>().tileNumberInList = (tempLavaList[i].GetComponent<Tile>().tileNumberInList) - 1;

                                    //It is a new LAVA, so add it to the lavaTiles list.
                                    tempLavaList.Add(tempObj);

                                    //Keep the Inspector clean, put all tiles inside of a parent in the hierarchy.
                                    tempObj.transform.SetParent(allTileParent);
                                }
                            }

                            if (tempLavaList[i].GetComponent<Tile>().tileNumberInList < xTiles * yTiles) {
                                //Check to the right of the tile that can propagate LAVA.
                                if (tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) + 1].name.Contains("EMPT")) {
                                    tempObj = Instantiate(lavaTile, tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) + 1].transform.position, Quaternion.identity) as GameObject;

                                    if (tempObj.transform.position == new Vector3(0.0f, 9.0f, 0.0f)) {
                                        tempObj.transform.position = marioWasLastSeenHere;
                                    }

                                    //Replace the EMPT tile in the tiles list with LAVA.
                                    tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) + 1] = tempObj;

                                    //Update the tileNumberInLavaList for the current added LAVA.
                                    tempLavaList[i].GetComponent<Tile>().tileNumberInList = (tempLavaList[i].GetComponent<Tile>().tileNumberInList) + 1;

                                    //It is a new LAVA, so add it to the lavaTiles list.
                                    tempLavaList.Add(tempObj);

                                    //Keep the Inspector clean, put all tiles inside of a parent in the hierarchy.
                                    tempObj.transform.SetParent(allTileParent);
                                }
                            }

                        } else { //It is EMPT below this current LAVA tile, so propagate the lava!
                                 //lavaTiles.Add(tempObj) wants an assigned GameObject.
                            GameObject tempObj;

                            tempObj = Instantiate(lavaTile, tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) + xTiles].transform.position, Quaternion.identity) as GameObject;

                            if (tempObj.transform.position == new Vector3(0.0f, 9.0f, 0.0f)) {
                                tempObj.transform.position = marioWasLastSeenHere;
                            }

                            //Replace the EMPT tile in the tiles list with LAVA.
                            tiles[(tempLavaList[i].GetComponent<Tile>().tileNumberInList) + xTiles] = tempObj;

                            //Update the tileNumberInLavaList for the current added LAVA.
                            tempLavaList[i].GetComponent<Tile>().tileNumberInList = (tempLavaList[i].GetComponent<Tile>().tileNumberInList) + xTiles;

                            //It is a new LAVA, so add it to the lavaTiles list.
                            tempLavaList.Add(tempObj);

                            //Keep the Inspector clean, put all tiles inside of a parent in the hierarchy.
                            tempObj.transform.SetParent(allTileParent);
                        }
                    }
                }

                //Replace the old lavaTiles list with the new list which has new lavaTiles appended to it.
                lavaTiles = tempLavaList;

                //Reset the timer.
                lavaTimerCurrent = lavaTimer;
            } else {
                lavaTimerCurrent -= Time.deltaTime;
            }
        }
    }
}