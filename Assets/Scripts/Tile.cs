﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {
    [HideInInspector]
    public int tileNumberInList;

    public float grndTimer = 0.5f;

    [HideInInspector]
    public float grndTimerCurrent;

    [HideInInspector]
    public List<Sprite> grndTileFrames = new List<Sprite>();

    [HideInInspector]
    public bool isGRNDTile = false, isEDGETile = false, isEMPTTile = false;

    //[HideInInspector]
    public int currentGRNDTileFrame = 0;

    private void Start() {
        grndTimerCurrent = grndTimer;
    }

    private void Update() {
        if (isGRNDTile) {
            if (grndTimerCurrent <= 0.0f) {
                if (currentGRNDTileFrame < grndTileFrames.Count) {
                    gameObject.GetComponent<SpriteRenderer>().sprite = grndTileFrames[currentGRNDTileFrame];
                    currentGRNDTileFrame++;
                    grndTimerCurrent = grndTimer;
                } else {
                    GameObject tempObj = Instantiate(TileManager.instance.EMPTTile, gameObject.transform.position, Quaternion.identity) as GameObject;
                    TileManager.instance.tiles[tileNumberInList] = tempObj;
                    tempObj.transform.SetParent(TileManager.instance.allTileParent);
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
