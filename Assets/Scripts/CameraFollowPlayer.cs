﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{
    [HideInInspector]
    public GameObject mario;
    public AudioClip gameOverMan;

    [HideInInspector]
    public bool playGameOverSoundEffect = false;
    
    void Update() {
        if(mario != null) {
            transform.position = new Vector3(mario.transform.position.x, mario.transform.position.y + 4.0f, transform.position.z);
        }

        if(playGameOverSoundEffect) {
            GetComponent<AudioSource>().Stop();

            GetComponent<AudioSource>().PlayOneShot(gameOverMan, 1.0f);
            playGameOverSoundEffect = false;
        }
    }
}
