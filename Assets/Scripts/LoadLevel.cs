﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevel : MonoBehaviour {

    private float xStartPos = 0.0f, yStartPos = 0.0f;
    private float tileSize = 1.0f;

    //[SerializeField]
    private string[] tilesCSVList;

    public List<GameObject> tileTypes = new List<GameObject>();

    public TMPro.TMP_Text tmpLivesRemaining;


    private void Start() {
        PlaceTiles("Level_1");
    }

    public static string[] ReadLevelTilesFromCSV(string levelTileSpawnCSV) {
        TextAsset csvtext = Resources.Load("Levels/" + levelTileSpawnCSV) as TextAsset;
        return csvtext.ToString().Split(new char[] { '\n', '\r', ',', ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
    }

    public void PlaceTiles(string levelToLoad) {

        tilesCSVList = ReadLevelTilesFromCSV(levelToLoad);

        //Centre the grid of tiles on the screen.
        if (TileManager.instance.xTiles % 2 == 0) {
            xStartPos = -((TileManager.instance.xTiles * tileSize) / 2) + ((tileSize / 2) + 0.5f);
        } else {
            xStartPos = -((TileManager.instance.xTiles * tileSize) / 2) + ((tileSize / 2));
        }

        if (TileManager.instance.yTiles % 2 == 0) {
            yStartPos = ((((TileManager.instance.yTiles * tileSize) / 2) + ((tileSize / 2) - 0.5f)) * 2.0f) - tileSize;
        } else {
            yStartPos = ((((TileManager.instance.yTiles * tileSize) / 2) + (tileSize / 2)) * 2.0f) - tileSize;
        }

        Vector3 currPos = new Vector3(xStartPos, yStartPos, 0.0f);

        //One-by-one, the tiles are placed.
        for (int y = 0; y < TileManager.instance.yTiles; ++y) {
            for (int x = 0; x < TileManager.instance.xTiles; ++x) {
                GameObject tempObj;

                tempObj = Instantiate(tileTypes.Find(tile => tile.name.Contains(tilesCSVList[(y * TileManager.instance.xTiles) + x])), currPos, Quaternion.identity) as GameObject;

                if (tempObj.name.Contains("MRIO")) {

                    TileManager.instance.marioWasLastSeenHere = currPos;

                    //Is there already a Mario in the level?
                    if (FindObjectsOfType<MarioManager>().Length > 1) {
                        //Kill the just-spawned Mario.
                        Destroy(tempObj);

                        tempObj = Instantiate(tileTypes.Find(tile => tile.name.Contains("EMPT")), currPos, Quaternion.identity) as GameObject;

                        //Make sure to still set that tile position as EMPT tile.
                        TileManager.instance.tiles.Add(tileTypes.Find(tile => tile.name.Contains("EMPT")));

                        //Now we don't need any of the below logic for this tile.
                        continue;
                    } else {
                        TileManager.instance.marioPositionInList = (y * TileManager.instance.xTiles) + x;
                    }
                }

                //If it is a lava tile, we want to put it into a separate list also, to handle all lava flowing checks easily and more efficiently than just checking the main list.
                if (tempObj.name.Contains("LAVA")) {
                    TileManager.instance.lavaTiles.Add(tempObj);
                    tempObj.GetComponent<Tile>().tileNumberInList = TileManager.instance.tiles.Count;
                }

                //If the tile is a GRND tile, we need to give Tile.cs the list of different grndTile frames, also we need  to state it is a ground tile.
                if (tempObj.name.Contains("GRND")) {
                    tempObj.GetComponent<Tile>().tileNumberInList = TileManager.instance.tiles.Count;
                    tempObj.GetComponent<Tile>().grndTileFrames = TileManager.instance.grndTileFrames;
                    tempObj.GetComponent<Tile>().isGRNDTile = true;
                }

                if(tempObj.name.Contains("END!")) {
                    tempObj.GetComponent<SpriteRenderer>().sortingOrder = -1;
                }

                if (tempObj.name.Contains("EMPT")) {
                    tempObj.GetComponent<Tile>().isEMPTTile = true;
                }

                if (tempObj.name.Contains("EDGE")) {
                    tempObj.GetComponent<Tile>().isEDGETile = true;
                }

                //This just helps the enemies look more realistically placed on the level.
                if (tempObj.name.Contains("XGND") || tempObj.name.Contains("XFLY")) {
                    tempObj.transform.position = new Vector3(tempObj.transform.position.x, tempObj.transform.position.y + 0.2f, tempObj.transform.position.z);
                }

                //Now that the enemies and Mario are spawned, we want the tile location they spawned on to be saved out in he tile reference list as "EMPT" instead of themselves.
                if (tempObj.name.Contains("XGND") || tempObj.name.Contains("XFLY") || tempObj.name.Contains("MRIO")) {
                    //Add an "Empty Tile" to the tiles list and spawn it.
                    tempObj = Instantiate(TileManager.instance.EMPTTile, currPos, Quaternion.identity) as GameObject;
                    TileManager.instance.tiles.Add(tempObj);
                } else {
                    //Add the tile to the tiles list.
                    TileManager.instance.tiles.Add(tempObj);
                }

                //Keep the Inspector clean, put all tiles inside of a parent in the hierarchy.
                tempObj.transform.SetParent(TileManager.instance.allTileParent);

                //Set the next X position for the next tile.
                currPos.x += tileSize;
            }

            //End of a row? Next Y position and start form first X position again.
            currPos = new Vector3(xStartPos, currPos.y - tileSize, 0.0f);
        }

        TileManager.instance.canExecute = true;
        Array.Clear(tilesCSVList, 0, tilesCSVList.Length);
    }
}
