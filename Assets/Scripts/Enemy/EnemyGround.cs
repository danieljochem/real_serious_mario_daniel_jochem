﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGround : MonoBehaviour {
    private bool goingLeft = true;
    private float movementSpeed = 0.02f;

    private float xPositionOfPreviousFrame = 0.0f;

    public GameObject fireballObj;
    public float timeToFireFireball = 2.0f;
    private float currentTimeToFireFireball;


    private void Start() {
        xPositionOfPreviousFrame = transform.position.x;
        currentTimeToFireFireball = timeToFireFireball;
    }

    void Update() {
        //Movement, side-to-side, when it runs into a block, it switches sides.
        if (goingLeft) {
            if (((gameObject.transform.position + (Vector3.left * movementSpeed)).x - xPositionOfPreviousFrame) != 0.0f) {
                //This Debug adds JUST the right amount of delay for the enemy to change directions at the correct time,
                //it is super bad to have Debug.Log's printing, but I think it may still be woth it...
                Debug.Log("These Debug.Log's help with consistency of switching directions, surprisingly..." /*((gameObject.transform.position + (Vector3.left * movementSpeed)).x - xPositionOfPreviousFrame).ToString()*/);
                gameObject.transform.position += (Vector3.left * movementSpeed);
                gameObject.GetComponent<SpriteRenderer>().flipX = true;
                xPositionOfPreviousFrame = transform.position.x;
            } else {
                goingLeft = false;
            }

        } else {
            if (((gameObject.transform.position + (Vector3.right * movementSpeed)).x - xPositionOfPreviousFrame) != 0.0f) {
                //This Debug adds JUST the right amount of delay for the enemy to change directions at the correct time,
                //it is super bad to have Debug.Log's printing, but I think it may still be woth it...
                Debug.Log("These Debug.Log's help with consistency of switching directions, surprisingly..." /*((gameObject.transform.position + (Vector3.right * movementSpeed)).x - xPositionOfPreviousFrame).ToString()*/);
                gameObject.transform.position += (Vector3.right * movementSpeed);
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
                xPositionOfPreviousFrame = transform.position.x;
            } else {
                goingLeft = true;
            }
        }

        //Instantiating a fireball on a timer.
        if(currentTimeToFireFireball <= 0.0f) {
            GameObject fireball = Instantiate(fireballObj, gameObject.transform.position, Quaternion.identity) as GameObject;
            fireball.GetComponent<SpriteRenderer>().sortingOrder = -1;
            currentTimeToFireFireball = timeToFireFireball;
        } else {
            currentTimeToFireFireball -= Time.deltaTime;
        }
    }
}
