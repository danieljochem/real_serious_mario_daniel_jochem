﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    private float movementSpeed = 0.25f;

    private void Update() {
        gameObject.transform.position += (Vector3.up * movementSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.name.Contains("EDGE")) {
            SelfDestruct();
        }
    }

    public void SelfDestruct() {
        //Explode


        //Destory
        Destroy(this.gameObject);
    }
}
