﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlying : MonoBehaviour {

    public GameObject mario;
    
    void Update() {
        if(mario != null) {
            //Vector2 direction = mario.transform.position - gameObject.transform.position;
            transform.position = Vector2.MoveTowards(gameObject.transform.position, mario.transform.position, Time.deltaTime * 1.5f);
        }
    }
}
